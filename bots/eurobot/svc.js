'use strict';
let AppStartTime = new Date(); 
console.log(AppStartTime);

// BABEL INIT
require("@babel/register");

// default libs used below
const fs = require('fs');

// app constant
const app = require('./lib/app').default;

// Discord consts
const Discord = require('discord.js');
const bot = new Discord.Client();
const RichEmbed = Discord.RichEmbed;

// Scheduler
const scheduler = require('node-schedule');

// Log channel reference
let ServerLogChannel;

//
// App object
//
let App = new app();

//
// LOCAL VARIABLES
//

// debate variables
let Debater = null;
let DebateTopic = null;
let Participants = [];

// personality
let nice = true; // enable responses

// shut up timer
let maxShutupTime = 600000; // ms to shut up

let shutupTimer = 0;

//
// Local SVC functions
//

// Publish the calendar from/to a datetime with a datestring preamble to a channel
let publishScheduledCalendar = function(calendarTo = null, calendarFrom = null,dateString = null,channelName = null){

	App.getGoogleCalendarItems(App.opt.calendar,calendarTo,calendarFrom)
	.then(items=>{

		const guild = bot.guilds.find(x=>x.name === App.opt.discord.guild);

		if(!channelName) channelName = App.opt.calendar.defaultCalendarChannel;

		if(guild && channelName) {
			
			let channel = guild.channels.find(ch => ch.name === channelName);
			if(channel) {

				let calendarString = '';
				let subscribeString = `**N.B.** Subscribe to ${App.opt.calendar.calendarLocation} or ask for !help.`

				if(dateString) calendarString = `Calendar for ${dateString}\n\n`;

				if(items.length > 0) {
	
					let itemLengthString = '';
					if(items.length > App.opt.calendar.maxItems) {
					
						let LenPos = items.length - App.opt.calendar.maxItems;
						itemLengthString = `... and ${LenPos} more.\n\n`;
						items.splice((-1 * LenPos),LenPos);

					}

					const calendar = App.formatCalendar(items);
					if(calendar) {
		
						calendarString = calendarString + calendar + itemLengthString + subscribeString; 
						
					}
		
				} else {

					calendarString = `\n**No entries found.**\n\n`+subscribeString;
					
				}

				let embed = new RichEmbed()
					.setTitle(`Forum :flag_eu: Götterfunken :flag_eu: Calendar`)
					.setDescription(calendarString)
					.setColor(0x003399);
				
				channel.send(embed);

			}	

		}

	})
	.catch(e=>{

		console.log(e);

	});

}

//
// BOT
//

// bot ready fn
bot.on('ready', () => {

	console.log(`Successfully connected to Discord as ${bot.user.tag}`);

	//
	// Interval triggers 
	//

	// MIDNIGHT CET CALENDAR
	App.Intervals.push(scheduler.scheduleJob(App.opt.discord.intervals.calendarMidnight,function(){

		publishScheduledCalendar(null,null,'today');

	}));

	// 09:00 CET CALENDAR (for today)
	App.Intervals.push(scheduler.scheduleJob(App.opt.discord.intervals.calendarMorning,function(){

		let calendarFrom = new Date(new Date().setHours(0,0,0,0)); 
		let calendarTo = new Date(calendarFrom.getTime() + (86400000 -1));

		publishScheduledCalendar(calendarTo,calendarFrom,'today');

	}));

	// 20:00 CET CALENDAR (for tomorrow)
	App.Intervals.push(scheduler.scheduleJob(App.opt.discord.intervals.calendarEvening,function(){

		let calendarFrom = new Date(new Date(new Date().getTime() + 86400000).setHours(0,0,0,0));
		let calendarTo = new Date(new Date(calendarFrom).getTime() + (86400000));
		
		publishScheduledCalendar(calendarTo,calendarFrom,'tomorrow');

	}));

	// set ServerLogChannel
	ServerLogChannel = bot.channels.find(c=>c.name === App.opt.discord.channels.serverLog);

});

// error: do not immediately throw, but output for log
bot.on('error',err=>{

	console.log(err);
	process.exit(1);

});

bot.on('disconnect',msg=>{

	if(msg) console.log(msg);
	process.exit(1)

});

// On User Joins
bot.on('guildMemberAdd',member=>{

	const embed = new RichEmbed()
		.setTitle(':flag_eu: Welcome to Forum Götterfunken! :flag_eu:')
		.setDescription(App.opt.responses.onJoinMsg)
		.setColor(0xFF0000);

	member.send(embed);

	const channel = member.guild.channels.find(ch => ch.name === 'general-effort'); 
	if(!channel) return;

	let emoji = App.parseGuildEmoji(
		App.opt.responses.botEmojiList[Math.round(Math.random() * (App.opt.responses.botEmojiList.length -1 ))],
		member.guild.emojis
	);

	channel.send(`${member} has joined. ${emoji}`);

});

// On User Removes
bot.on('guildMemberRemove',member=>{

	const channel = member.guild.channels.find(ch => ch.name === 'server-log'); 
	if(!channel) return;
	channel.send(`${member} has left.`);

});

//Bot reaction proc
bot.on("messageReactionAdd", (messageReaction, user) => {

	if (messageReaction.emoji.name === '❌'){

		let mods = messageReaction.message.guild.members.filter(member => { 
			return member.roles.find(r => r.name === 'Moderator');
		}).map(member => {
			return member.user.id;
		})

		let tribunes = messageReaction.message.guild.members.filter(member => { 
			return member.roles.find(r => r.name === 'Tribune');
		}).map(member => {
			return member.user.id;
		})

		let wardens = messageReaction.message.guild.members.filter(member => { 
			return member.roles.find(r => r.name === 'Warden');
		}).map(member => {
			return member.user.id;
		})

		if(mods.includes(user.id) || tribunes.includes(user.id)){

			let member = user.username;
			let author = messageReaction.message.author.username;
			let message = messageReaction.message;

			App.remove(member, author, message)
				.then(embed=>{
				
					if(ServerLogChannel) ServerLogChannel.send(embed);

				});

		}

		if(wardens.includes(user.id) && messageReaction.message.channel.name && messageReaction.message.channel.name === "debates"){

			let member = user.username;
			let author = messageReaction.message.author.username;
			let message = messageReaction.message;

			App.remove(member, author, message)
				.then(embed=>{
				
					if(ServerLogChannel) ServerLogChannel.send(embed);

				});
		
		}

	}

});

// Message proc
bot.on('message', msg => {

	if(msg.guild && msg.guild.roles) {

		// only in channels, not by bot
		if(
			msg.channel && 
			msg.member && 
			msg.member.user && 
			msg.member.user.username && 
			msg.member.user.username.toLowerCase() !== 'eurobot' && 
			msg.member.user.username.toLowerCase() !== 'eurobotti' && 
			App.kwdTimeout === false
		) {

			// message content for checking keywords
			let contentLower = msg.content.toLowerCase();
			
			// bot personality
			if(nice && shutupTimer < ((new Date()).getTime() - maxShutupTime && (msg.channel.name && !['announcements','moderation'].includes(msg.channel.name)))) {

				// default mention of eurobot
				if (msg.isMemberMentioned(bot.user) || contentLower.includes('eurobot')) {

					// keyword timer
					App.kwdTimerInit();
				
					setTimeout(()=>{

						let emoji = App.parseGuildEmoji(
							App.opt.responses.botEmojiList[(Math.round(Math.random() * (App.opt.responses.botEmojiList.length-1)))],
							msg.guild.emojis
						);
						if(emoji) {

							// message or emoji?
							if (Math.round(Math.random()) === 1) {

								let reactionMsg = App.opt.responses.mentionReactionMessage[(Math.round((App.opt.responses.mentionReactionMessage.length-1) * Math.random()))]; 
								let msgRtn = `${reactionMsg} ${emoji}`;

								// emoji first or last?
								if(Math.round(Math.random()) === 1) msgRtn = `${emoji} ${reactionMsg}`;

								msg.channel.send(msgRtn);

							} else {

								msg.channel.send(`${emoji}`);		

							}

						}

					},Math.floor(600*Math.random())+1000);

				}

				// Flag react
				if(msg.content === '🇪🇺') msg.react('🇪🇺');

				// FREUDE SCHONER GOTTERFUNKEN responses
				if(msg.content === 'FREUDE' || msg.content === 'FREUDE!' || msg.content === 'freude') {

					let gachi = App.parseGuildEmoji('eurogachi',msg.guild.emojis);
					msg.channel.send(`SCHÖNER! ${gachi}`);

				}

				if(msg.content === 'GÖTTERFUNKEN' || msg.content === 'GOTTERFUNKEN') {
					msg.channel.send(`:flag_eu:`);
				}

				// set shutup timer
				if(App.matchStringStartToArray(msg.content,App.opt.responses.shutupCommands)) {

					shutupTimer = (new Date()).getTime();
					msg.channel.send(`:zipper_mouth: ok`);

				}

			} // personality

			// Retrieve calendar
			if(contentLower.startsWith('!calendar')) {

				// keyword timer
				App.kwdTimerInit();


				let calendarFrom = null; 
				let calendarTo = null;
				let calendarString = null;

				let splitMsg = msg.content.split(' ');
				if(splitMsg.length > 1) {

					let timeString = splitMsg[1];

					if(splitMsg.length > 2) {

						let joinedSplit = splitMsg;
						joinedSplit.shift();
						let concatted = joinedSplit.join(' ');

						timeString = concatted;

					}

					if(timeString.includes('today')) {

						calendarFrom = new Date(new Date().setHours(0,0,0,0)); 
						calendarTo = new Date(calendarFrom.getTime() + (86400000 -1));
						calendarString = 'today';

					}

					if(timeString.includes('tomorrow')) {

						calendarFrom = new Date(new Date(new Date().getTime() + 86400000).setHours(0,0,0,0));
						calendarTo = new Date(new Date(calendarFrom).getTime() + (86400000));
						calendarString = 'tomorrow';

					}

					if(timeString.includes('this week')) {

						calendarFrom = new Date(new Date().setHours(0,0,0,0));
						calendarTo = new Date(calendarFrom.getTime() + (86400000 * 7) - 1);
						calendarString = 'this week';

					}

					if(timeString.includes('next week')) {

						calendarFrom = new Date(new Date(new Date().getTime() + (86400000 * 7)).setHours(0,0,0,0));
						calendarTo = new Date(calendarFrom.getTime() + (86400000 * 7) - 1);
						calendarString = 'next week';

					}
				
				}

				publishScheduledCalendar(calendarTo,calendarFrom,calendarString,msg.channel.name);

			}

			// help
			if(contentLower.startsWith('!help')) {

				// keyword timer
				App.kwdTimerInit();

				let description = App.opt.responses.helpDescription;
				if(msg.member && msg.member.roles && msg.member.roles.find(r=>r.name === 'Moderator')) description += App.opt.responses.helpModDescription;
				description += App.opt.responses.helpPostscript;

				let embed = new RichEmbed()
					.setTitle(':flag_eu: Forum Götterfunken Help :flag_eu:')
					.setDescription(description)
					.setColor(0xFF0000);

				msg.member.send(embed);

			}

			// !country
			if (contentLower.startsWith('!country') || contentLower.startsWith('!nation')) {

				// keyword timer
				App.kwdTimerInit();

				let list = App.getMsgOpts(msg.content,{capitalize:true});
				if(list) {
					
					let cntIDX = App.opt.whitelist.COUNTRY.findIndex(x=>x.name.toLowerCase() === list[0].toLowerCase());
					
					if(cntIDX > -1) {

						let cObj = App.opt.whitelist.COUNTRY[cntIDX];
						if(['Uk','Usa'].includes(cObj.name)) cObj.name = cObj.name.toUpperCase();

						if(msg.member && msg.member.roles && msg.member.roles.find(r=>r.id === cObj.id)) {

							msg.member.removeRole(cObj.id);
							if(msg.channel) msg.channel.send(`${msg.member} no longer belongs in ${cObj.name}`);

						} else {

							msg.member.addRole(cObj.id);
							if(msg.channel) msg.channel.send(`${msg.member} now belongs in ${cObj.name}`);

						}

					} else {

						if(msg.channel) msg.channel.send(`No such country, ${msg.member}`);

					}

				}

			}

			// User management
			if(msg.member && msg.member.roles && msg.member.roles.find(r=>['Tribune','Moderator','Warden'].includes(r.name))) {

				let mention = msg.mentions.members.first(),
					author = msg.member.user.username;

				// Ban only if Moderator or Tribune (can't ban your own type)
				if(contentLower.startsWith('!ban') && (msg.member.roles.find(r=>r.name === 'Moderator' || (msg.member.roles.find(r=>r.name === 'Tribune') && !mention.roles.find(r=>r.name === 'Tribune'))))){

					App.ban(mention,author)
						.then(embed=>{

							if(ServerLogChannel) ServerLogChannel.send(embed);

						});

				}

				// Kick only if Mod or Tribune or Warden (can't ban your own type)
				if(contentLower.startsWith('!kick') && (msg.member.roles.find(r=>r.name === 'Moderator') || (msg.member.roles.find(r=>r.name === 'Tribune') && !mention.roles.find(r=>r.name === 'Tribune')) || (msg.member.roles.find(r=>r.name === 'Warden') && !member.roles.find(r=>r.name === 'Tribune') && !mention.roles.find(r=>r.name === 'Warden')))) {

					App.kick(mention,author)
						.then(embed=>{

							if(ServerLogChannel) ServerLogChannel.send(embed);

						});

				}

				// MUTE / UNMUTE
				if( contentLower.startsWith('!mute') || contentLower.startsWith('!unmute') ) {

					let mentions = App.getMentions(contentLower);
					let muted = [],	unmuted = [];

					mentions.forEach(UserId=>{

						let mentionedMember = msg.guild.members.get(UserId);
						const muteRole = msg.guild.roles.find(r=>r.name.toLowerCase() ==='mute');
						
						if(mentionedMember && muteRole) {

							let toggle = App.toggleRole(mentionedMember,muteRole);

							toggle ? muted.push(mentionedMember.user.username) : unmuted.push(mentionedMember.user.username);
						
						}
					
					});

					if(muted.length > 0 || unmuted.length > 0) {

						let listMuted = muted.join(', ');
						let listUnmuted = unmuted.join(', ');

						let notice = '';
						
						notice += (listMuted ? 'Muted: '+ listMuted +'\n': '');
						notice += (listUnmuted ? 'Unmuted: '+ listUnmuted +'\n': '');

						if(notice.length > 0) {
							let embed = new RichEmbed()
								.setTitle('Mute by '+ msg.member.user.username)
								.setDescription(notice)
								.setColor(0x003399);

							let channel = bot.channels.find(c=>c.name === 'server-log');
							if(channel) channel.send(embed);

						}

					}

				}

				// Debates Channel Specific
				if(msg.channel.name && msg.channel.name === 'debates') {

					const channel = bot.channels.find(c=>c.name === 'debates');

					// DEBATETOPIC
					if(contentLower.startsWith('!topic')) { 

						let topic = msg.content.split(' ');
						let embed = new RichEmbed();
						
						embed.setColor(0x8730cf);

						if(topic.length > 1) {

							topic.shift();
							DebateTopic = topic.join(' ');
							embed.setTitle(`New debate topic`)
								.setDescription(DebateTopic);

						} else {

							DebateTopic = null;
							embed.setTitle(`Debate topic removed`);

						}

						if(channel && embed) channel.send(embed);

					}

					// DEBATE / UNDEBATE
					if(contentLower.startsWith('!floor')) {

						let mentions = App.getMentions(contentLower);
						if(mentions.length > 0) {


							const mentionedMember = msg.guild.members.get(mentions[0]);
							if(mentionedMember) {

								let topic = 'General Discussion';
							
								let embedRemove = new RichEmbed();
								let embedAdd = new RichEmbed();
								
								if(DebateTopic) topic = DebateTopic; 
								
								embedRemove.setTitle(topic)
									.setColor(0x8730cf);
								embedAdd.setTitle(topic)
									.setColor(0x8730cf);
	
								if(Debater) {
	
									embedRemove.setDescription(`${Debater.user.username} has relinquished the floor`);
									Debater = null;
	
									if(channel && embedRemove) channel.send(embedRemove);
	
								}
	
								Debater = mentionedMember;
	
								embedAdd.setDescription(`${mentionedMember.user.username} has the floor`);
	
								if(channel && embedAdd) channel.send(embedAdd);
	
							}

						}
				
					}

					// SET PARTICIPANTS
					if(contentLower.startsWith('!participant')) {

						let mentions = App.getMentions(contentLower);
						if(mentions.length > 0) {
							
							const debateRole = msg.guild.roles.find(r=>r.name.toLowerCase() ==='debate participant');

							mentions.forEach(mention=>{

								const mentionedMember = msg.guild.members.get(mention);

								let isPart = Participants.indexOf(mentionedMember.user.id);
								if(isPart < 0) {

									Participants.push(mentionedMember.user.id);
									mentionedMember.addRole(debateRole.id);

								}

							});

						}

					}

					// CLOSE DEBATE
					if(contentLower.startsWith('!close')) {

						let topic = 'General Discussion';
						if(DebateTopic) topic = DebateTopic;

						if(Participants.length>0) {

							const debateRole = msg.guild.roles.find(r=>r.name.toLowerCase() ==='debate participant');
							if(debateRole) {

								Participants.forEach(participantID=>{

									const mentionedMember = msg.guild.members.get(participantID);
									if(mentionedMember) mentionedMember.removeRole(debateRole.id);

								});

								Participants = [];

							}

						}

						let embed = new RichEmbed();
						embed.setTitle('Debate Closed')
							.setDescription(`${topic} has been closed by ${msg.member.user.username}`)
							.setColor(0x8730cf);

						if(channel && embed) channel.send(embed);

						DebateTopic = 'General Discussion';

					}

				}


			}

			// Debates channel specific
			if(msg.channel.name && msg.channel.name === 'debates') {

				//Relinquish
				if(contentLower.startsWith('!relinquish') && Debater && Debater.user && Debater.user.id === msg.member.id) {
					
					let topic = 'General Discussion';
					let embedRemove = new RichEmbed();
					const channel = bot.channels.find(c=>c.name === 'debates');

					if(DebateTopic) topic = DebateTopic; 
						
					embedRemove.setTitle(topic)
						.setColor(0x8730cf);

					embedRemove.setDescription(`${Debater.user.username} has relinquished the floor`);
					
					Debater = null;

					if(channel && embedRemove) channel.send(embedRemove);

				}

			}

		} // msg.channel

	}

});

//
// Init
//

bot.login(App.opt.discord.token);