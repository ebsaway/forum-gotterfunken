module.exports = {

    shutupCommands: [
        'shut up eurobot',
        'shutup eurobot',
        'eurobot, stop',
        'please stop eurobot',
        'fuck off eurobot',
        'fuckoff eurobot',
        'shut the fuck up eurobot',
        'now now eurobot',
        'eurobot shush',
        'eurobot stahp',
        'shush you bot',
        'stfu bot',
        'stfu eurobot',
    ],
    
    botEmojiList: [
        'pepeEU',
        'sassholi',
        'uwusconi',
        'vestagerthink',
        'Macronwink',
        'Merkelwhat',
        'eurozone',
        'michelpain',
        'tusk',
        'uschiface',
        'ohno',
        'oktajani',
        'gretaface',
        'dombrovskis',
        'okrutte',
        'mogherini',
        'freescotland',
        'timmerdaddy',
        'orbanlel',
        'skapocalypse',
        'higgins',
        'impressedhoney',
        'betteldaddy',
        'barnierwat',
        'conte',
        'borrell'
    ],
    
    // default mention reaction
    mentionReactionMessage: [
        '\\*cough\\*',
        '\\*ahem\\*',
        'hrm',
        'tsk',
        'wat',
        '\\*sniff\\*',
        'heh',
        'hmpf',
        'reee',
        'oh you',
        'hah',
        'huh',
        'eh?',
        'grr',
        'Freude',
        'pay debnts',
        'FREUDE',
        'uwu',
        'owo',
        'hrrrm',
        'pewpew',
        'hm?',
        'FREUDE!',
    ],
    
    // !help descriptions 
    helpDescription: `

**Commands**

eurobot has a number of global commands..

- change country role: \`!country <country name>\`
- ask for a calendar: \`!calendar\` (or \`!calendar <today,tomorrow,this week, next week>\`)`,

    helpModDescription: `

Mod commands: coming soon.`,
    
    helpPostscript: `
    
Enjoy :)
    
    `,
    
    onJoinMsg: `
    
**About Us **

Forum Götterfunken (FG) is a hub for activism, political education, and discussion related to Europe and the European Union. 

Our primary mission is to foster a fertile environment for creating and sustaining constructive activist projects.

We also act as an umbrella organization for a number of subreddits, websites and Discord servers tied to spreading awareness about the European Union and global affairs surrounding it.

**The Rules **

• No discriminatory language (no racism, sexism, antisemitism, ableism, homophobia, etc.)
• No NSFW content (no content that isn't safe for work)
• No spam (no ads, solicitation, links to other servers)
• No flooding (no excessive posting)
• No doxxing (revealing personal information without the person’s consent)
• No brigading (no posting links or screenshots with the purpose of defaming or taking action against users on any social media platforms)

Sanctions such as restrictions, bans and kicks are at the discretion of the mods. Appeal will be handled by the moderator on a case by case basis.

**Furthermore** 

• Please use the channels right; #low-effort for off-topic conversation, #serious-effort for in-depth discussion and Acropolis Europa for hobbies and interests.
• Be nice. Expect memes.

For more commands and help with user roles please use \`!help\`.
    `

};