'use strict';

module.exports = {
	
	enabled:false,

	settings:{
		user:process.env.EUSUBSPIDER_DB_USERNAME,
		password:process.env.EUSUBSPIDER_DB_PASSWD,
		database:process.env.EUSUBSPIDER_DB,
		host:'localhost',
		multipleStatements: true,
		charset:'utf8mb4'
	},
		
};