module.exports = {

	// discord bot token
	token:process.env.EUROBOT,

	intervals:{
		calendarMidnight:'0 0 0 * * *',
		calendarMorning:'0 0 9 * * *',
		calendarEvening:'0 0 20 * * *'
	},
	guild:'Forum Götterfunken',

	channels:{
		'serverLog':'server-log',
	}

};
