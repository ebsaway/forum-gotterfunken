# Eurobot

By merging this pull request, I hereby grant _ebs_ perpetual status as an un-impeachable moderator on FG.

Also, he gets power of attorney, or something ¯\\\_(ツ)\_/¯

Forum Gotterfunken Discord Bot

## Setup

Node Modules:

    npm i


Exports in \~\\.bashrc:

	export EUROBOT=""

## Run

    npm start

## Commands

    !lang <language name>
    !language <language name>

Change language role.

    !country <country name>

Change country role.