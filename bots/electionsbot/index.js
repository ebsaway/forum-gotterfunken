// Discord consts
const Discord = require('discord.js');
const bot = new Discord.Client();
const token = 'NjUwNDM0Nzk4NTU2MjgyOTIw.Xewl3w.CuzvrWEzNsm4MdNl6UqmvUjJhYc';
const fs = require('fs');
const RichEmbed = Discord.RichEmbed;

// 
let link = "http://j.mp/2DSCRKe" // private devices link
let botname = "ElectionBot"

let votingCodesNotUsed;
try {
	//load from the file into memory
	votingCodesNotUsed = JSON.parse(fs.readFileSync('./votingCodesNotUsed.json', 'utf8'));
} catch(e) {
	//votingCodesNotUsed = [];
	console.log(e);
}

let usedVotingCodes;
try {
	//load from the file into memory
	usedVotingCodes = JSON.parse(fs.readFileSync('./usedVotingCodes.json', 'utf8'));
} catch(e) {
	//usedVotingCodes = [];
	console.log(e);
}

//FUNCTIONS
try {

	function registerToVote(user){
		//getting an unused code
		let c = votingCodesNotUsed.pop().code;
		//sending the code to the author
		user.send("This is your code: "+ c + " to use here to vote: "+link);

		//removing the code from the json file
		fs.writeFileSync('./votingCodesNotUsed.json', JSON.stringify(votingCodesNotUsed));

		//creating element to push into the usedcodes file
		let uName = user.username;
		let uID = user.id;
		let registration = {
			code : c,
			username : uName,
			userID : uID
		};

		//adding the element to the list and updating the json file
		usedVotingCodes.push(registration);
		fs.writeFileSync('./usedVotingCodes.json', JSON.stringify(usedVotingCodes));
	}

	// bot online
	bot.on('ready', () =>{
		console.log('This bot is online');
	});

	// Bot channel/dm message proc
	bot.on('message', msg=>{
		if(msg.guild && msg.guild.roles) {

			// only in channels
			if(msg.channel && msg.member && msg.member.user && msg.member.user.username && msg.member.user.username.toLowerCase() !== botname) {

				//register to vote
				if(msg.content.toLowerCase().startsWith('!registertovote') || msg.content.toLowerCase().startsWith('!register')){
					if(votingCodesNotUsed.length > 0) {
						if(!usedVotingCodes.some(item => item.userID === msg.member.user.id)) {
							registerToVote(msg.author);
						}
						else {
							msg.author.send("You have already requested a code, if you didn't receive one contact the mods.");
						}
					}
					else{
						msg.channel.send("No more codes left, contact a mod to fix the issue");
					}
				}
			}
		}
	});

	bot.login(token);

} catch(e) {

	console.log(e);
	process.exit(1);

}