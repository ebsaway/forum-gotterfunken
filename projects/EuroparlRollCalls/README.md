# European Parliament Roll Call Votes

The European Parliament's roll call votes are the easiest way to know where MEPs stand.

In this folder you can find the vote records for the 8th session of the european parliament (2014-2019) divided by MEP as well as by Group

In addition to this you will find:
+ A table detailing each MEP's nationality as well as political group at the dissolution of the 8th
parliament
+ A table Linking each vote to its description and, where applicable, the document being voted on
+ A table linking each document with its legislative procedure and a link to the procedure on the 
parliament's legislative observatory
+ A table linking each procedure to a number of subject codes, detailing the areas of EU policy each vote impacted
+ A list of subject codes with a brief description

Currently only the data for the 8th parliament is available, some examples on how to use the data are provided in the `Example` jupyter notebook.

You can preview the notebook [here!](https://nbviewer.jupyter.org/urls/bitbucket.org/LRuffati/gotterfunken-clone/raw/6efb3d4d910d7bab19ee5390fc03a7127c2d7a73/EuroparlRollCalls/8thParl/Examples.ipynb)
